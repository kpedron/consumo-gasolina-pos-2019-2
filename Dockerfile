FROM registry.gitlab.com/kpedron/consumo-gasolina-pos-2019-2:dependencies

COPY . $APP_PATH

RUN mkdir -p assets/static \
  && python manage.py collectstatic --noinput

#ENTRYPOINT ['python', '/usr/src/app/manage.py', 'runserver', '0.0.0.0:8000']
